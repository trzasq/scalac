import React from 'react';
import renderer from 'react-test-renderer';

import Users from '../../src/js/Users';

// needed because this:
// https://github.com/facebook/jest/issues/1353
jest.mock('react-dom');

test('App renders', () => {
  const component = renderer.create(
    <Users />
  );
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
