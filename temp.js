let pagesCount = (response.headers.link !== undefined) ? response.headers.link.match(/&page=([0-9]+)/)[1] : 0;
let pagesArray = Array.from(Array(Number(pagesCount)).keys())
axios.all(pagesArray.map((index) => {
  let pagesNum = index + 1;




import axios from 'axios';
import { uniqBy } from 'lodash';
const baseUrl = 'https://api.github.com/';
const token = '944384f6dd04e4b14c30c87012cad68405f6425b';

export const GET_USERS_DONE = 'GET_USERS_DONE';
export const GET_REPOS_DONE = 'GET_REPOS_DONE';
export const TOGGLE_SORT_DIRECTION = 'TOGGLE_SORT_DIRECTION';
export const SET_USER_DETAILS_ID = 'SET_USER_DETAILS_ID';
export const SET_REPO_DETAILS_ID = 'SET_REPO_DETAILS_ID';

export const getUsersDone = (users) => ({ type: GET_USERS_DONE, users });
export const getReposDone = (repos) => ({ type: GET_REPOS_DONE, repos });
export const toggleSortDirection = (direction) => ({ type: TOGGLE_SORT_DIRECTION, direction });
export const setUserDetailsId = (id) => ({ type: SET_USER_DETAILS_ID, id });
export const setRepoDetailsId = (id) => ({ type: SET_REPO_DETAILS_ID, id });


export const getAllAngularRepos = () => {
  let usersArray = [];
  let reposArray = [];
  let users;
  let contributors;

  return (dispatch) => {
    const range = ((n) => [...Array(n).keys()]);
    axios.head(`${baseUrl}orgs/angular/repos`, {
      params: { per_page: 100 },
      headers: { "Authorization": `token ${token}` }
    }).then(response => {
      if (response.status === 200 || response.status === 304) {

        const pagesNum = response.headers.link.match(/&page=([0-9]+)/)[1];
        const pagesNumArray = Array.from(Array(Number(pagesNum)).keys())

        // axios.all(response.data.map((result) => {
        axios.all(pagesNumArray.map((index) => {
          let page = index + 1;
          return axios.get(`${baseUrl}orgs/angular/repos`, {
            params: { per_page: 100, page },
            headers: {
              "Authorization": `token ${token}`
            }
          })
        })).then(response => {
          let mergeResults = [];
          response.map(resp=>{
            if (resp.status === 200 || resp.status === 304) {
              mergeResults = [...mergeResults, ...resp.data]
            }

           });
            axios.all(mergeResults.map((result) => {


              return axios.get(`${baseUrl}repos/angular/${result.name}/contributors`,
                {params: { per_page: 100}, headers: { "Authorization": `token ${token}` } }

              )

                .then(response => {
                  if (response.status === 200 || response.status === 304) {

                    let ids = [];
                    let usersIds = response.data.map(user => {
                      ids.push(user.id);
                      result.usersIds = ids;
                    })

                    reposArray = [...reposArray, { ...result }]
                    usersArray = [...usersArray, ...response.data];
                  }
                  return users
                })
            })).then(res => {
              users = usersArray.map(item => {

                item.angularContributions = 1;
                return item;
              })
              contributors = new Map(); users.forEach((element) => { if (contributors.get(element.id)) contributors.set(element.id, contributors.get(element.id) + element.angularContributions); else contributors.set(element.id, element.angularContributions); });
              users = _.uniqBy(users, 'id');
              users.map(user => {

                user.angularContributions = contributors.get(user.id);
                return users

              });
              dispatch(getUsersDone(users))
              dispatch(getReposDone(reposArray))

            }).catch((error) => {
              // dispatch(getDataFailed(error.message));
            });



        })


      }
    });

    //TODO

  }
}

// export const getAllAngularRepos = () => {
//   let array = [];
//   return (dispatch) => {
//     axios.get(`./service/angular/repos`) //TODO
//       .then(response => {
//         if (response.status === 200) {

//           return response;
//         }
//         //     return <GitUser key={response.data.id} name={response.data.name}/>;
//       })
//       .then((result) => {
//         // console.log(result.data)
//         // let unique =  _.uniqBy(result.data, 'id');
//         let users = result.data.map(item => {
//           item.contributions = 1;
//           return item;
//         })

//        let  contributors = new Map(); result.data.forEach((element) => { if (contributors.get(element.id)) contributors.set(element.id, contributors.get(element.id) + element.contributions); else contributors.set(element.id, element.contributions); }); 
//         users =  _.uniqBy(users, 'id');
//        users.map(user =>{
//          user.contributions = contributors.get(user.id);
//          return user

//        })



//         // console.log(unique);
//         dispatch(getUsersDone(users))
//       }).catch((error) => {
//         console.log(error)
//         // dispatch(getDataFailed(error.message));
//       });

//   }


// }