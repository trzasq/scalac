import {combineReducers} from 'redux';
import usersReducer from './users';
import utilsReducer from './utils';


const rootReducer = combineReducers({
  usersReducer,
  utilsReducer
});

export default rootReducer;