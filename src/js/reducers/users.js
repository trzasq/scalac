import {
  GET_USERS_DONE,
  GET_REPOS_DONE,
  TOGGLE_SORT_DIRECTION,
  SET_USER_DETAILS_ID,
  SET_REPO_DETAILS_ID,
  SET_FILTER_TYPE,
} from '../actions/usersActions';

const usersReducer = (state = {
  listLimit: 100,
  users: [],
  repos: [],
  selectedUser: null,
  selectedRepo: null,
  sort: {type: "angularContributions", direction: "desc"}
}, action) => {
  switch (action.type) {
    case GET_USERS_DONE:
      return {
        ...state,
        users: action.users
      };
    case GET_REPOS_DONE:
      return {
        ...state,
        repos: action.repos
      };
    case TOGGLE_SORT_DIRECTION:
      return {
        ...state,
        sort: {...state.sort, direction: action.direction}
      };
    case SET_USER_DETAILS_ID:
      return {
        ...state,
        selectedUser: action.id
      };
    case SET_REPO_DETAILS_ID:
      return {
        ...state,
        selectedRepo: action.id
      };
    case SET_FILTER_TYPE:
     return {
        ...state,
        sort: {...state.sort, type: action.filterName}
      };
    default:
      return state;
  }
};

export default usersReducer;