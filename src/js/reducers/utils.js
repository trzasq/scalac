import {
  GET_DATA_FAILED,
  CLEAR_ERRORS,
  HIDE_LOADING,
  SHOW_LOADING,
} from '../actions/utilsActions';
import {
  GET_USERS_DONE
} from '../actions/usersActions';

const utilsReducer = (state = {
  error: {
    show: false,
    message: ''
  },
  isLoading: false
}, action) => {
  switch (action.type) {
    case GET_DATA_FAILED:
     return {
        ...state,
        error: {...state.error, show: true, message: action.message},
        isLoading: false
      };
    case CLEAR_ERRORS:
     return {
        ...state,
        error: {...state.error, show: false, message: ''}
      };
    case SHOW_LOADING:
     return {
        ...state,
        isLoading: true
      };
    case HIDE_LOADING:
     return {
        ...state,
        isLoading: false
      };
    case GET_USERS_DONE:
     return {
        ...state,
        isLoading: false
      };
    default:
      return state;
  }
};

export default utilsReducer;