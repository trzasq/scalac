import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import {clearError} from './actions/utilsActions';
import App from 'grommet/components/App';
import Anchor from 'grommet/components/Anchor';
import Box from 'grommet/components/Box';
import Header from 'grommet/components/Header';
import Menu from 'grommet/components/Menu';
import Split from 'grommet/components/Split';
import Sidebar from 'grommet/components/Sidebar';
import Title from 'grommet/components/Title';
import Toast from 'grommet/components/Toast';

import About from './Components/About';
import Users from './Components/Users';
import UserDetails from './Components/UserDetails';
import RepositoryDetails from './Components/RepositoryDetails';
import NotFound from './Components/NotFound';
import Notes from './Components/Notes';
import Preloader from './Components/Preloader';

class BasicApp extends Component {

  constructor(){
    super();

    this._onCloseNotification = this._onCloseNotification.bind(this);
  }

  _onCloseNotification() {
    this.props.dispatch(clearError());

  }
  render() {
    const { error, isLoading } = this.props;
    return (
      <Router>
        <App centered={false}>
          <Split showOnResponsive='both' fixed={true} flex='right'>
            <Sidebar size="small" full={true} colorIndex="neutral-1">
              <Header pad='medium' justify='between'>
                <Title >
                  <Anchor label='ScalaC' href="#"></Anchor>
                </Title>
              </Header>
              <Menu primary={true}>
                <Anchor
                  path={{
                    path: '/',
                    index: true
                  }}>
                  Start

                </Anchor>
                <Anchor
                  path={{
                    path: '/users',
                    index: true
                  }}>
                  Angular Contributors
                </Anchor>
                <Anchor
                  path={{
                    path: '/notes',
                    index: true
                  }}>
                  Release Notes
                </Anchor>

              </Menu>

            </Sidebar>

            <Box full={true}>
              <Route
                render={({ location, history, match }) => {
                  return (
                    <Switch key={location.key} location={location}>
                      <Route exact path='/' component={About} />
                      <Route exact path='/notes' component={Notes} />
                      <Route path='/users' component={Preloader()} />
                      <Route path="/user/:id" component={UserDetails} />
                      <Route path="/repo/:id" component={RepositoryDetails} />
                      <Route component={NotFound} />
                    </Switch>
                  );
                }} />

            </Box>
          </Split>
          {error.show && <Toast status='critical'
            onClose={this._onCloseNotification}>
            {error.message}
          </Toast>}
        </App>
      </Router>
    );
  }
}

const mapStateToProps = (state) => ({
  error: state.utilsReducer.error,
})

export default BasicApp = connect(mapStateToProps)(BasicApp);


