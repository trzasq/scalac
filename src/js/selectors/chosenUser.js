import { createSelector } from 'reselect';

const usersSelector = state => state.usersReducer.users;

const chosenUserSelector = state => state.usersReducer.selectedUser;

const showUser = (users, selectedUser) => {

  const chosenUser = users.filter(user=>{
      
   return user.id == selectedUser;
 })[0];
 return chosenUser;
}

 export const userSelector =  createSelector (
  usersSelector,
  chosenUserSelector,
  showUser
)
