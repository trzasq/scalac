import {createSelector} from 'reselect';
import {filterSelector} from './sortedList'

const userSelector = state => state.usersReducer.selectedUser;
const usersSelector = state => state.usersReducer.users;
const repoIdSelector = state => state.usersReducer.selectedRepo;
const reposSelector = state => state.usersReducer.repos;

const filterRepos = (repos, filter) => {

  const filteredRepos = repos.filter(repo => {
    return (repo
      .usersIds !== undefined) ? 
      repo.usersIds.includes(Number(filter)): false;
  });

  return filteredRepos;
}

const getSelectedRepo = (repos, selected) => {

  const selectedRepo = repos.filter(repo => {
    return repo.id == selected
  })[0];

  return selectedRepo
}

const filterUsersFromSelectedRepo = (specificRepo, users) => {
  const usersList = users.filter(user => {
    return specificRepo
      .usersIds
      .includes(user.id)
  });
  return usersList;

}

export const userReposSelector = createSelector(reposSelector, userSelector, filterRepos);

export const specificRepoSelector = createSelector(reposSelector, repoIdSelector, getSelectedRepo);

export const usersBelongsToSpecificRepo = createSelector(specificRepoSelector, filterSelector, filterUsersFromSelectedRepo);
