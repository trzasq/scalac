import { createSelector } from 'reselect';

const limitSelector = state => state.usersReducer.listLimit
const usersSelector = state => state.usersReducer.users;
const sortSelector = state => state.usersReducer.sort;

const sortUsers = (users, sort) => {
 const filteredUsers = (sort.direction === 'desc') ? users.sort((a, b) => {
  return  a[sort.type] - b[sort.type]}).reverse() : users.sort((a, b) => {
  return  a[sort.type] - b[sort.type]})
  return filteredUsers;
}

const sliceUsers = (users, limit) => {
  const slicedUsers = users.slice(0, limit)
return slicedUsers;
}

 export const filterSelector =  createSelector (
  usersSelector,
  sortSelector,
  sortUsers
);

export const sliceSelector = createSelector (
    filterSelector,
    limitSelector,
    sliceUsers
)


