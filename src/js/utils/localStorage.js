export const loadState = () => {
  try {
    const serializedState = localStorage.getItem('state');
    if (serializedState === null) {
      return undefined;
    }
    return JSON.parse(serializedState);
  } catch (err) {
    return undefined;
  }
};

export const saveState = (state) => {

  try {
    const {usersReducer, utilsReducer} = state;
    let newState = {};
    newState = {
      ...newState,
      usersReducer,
      utilsReducer
    };
    const serializedState = JSON.stringify(newState);
    localStorage.setItem('state', serializedState);
  } catch (err) {
    console.log(err);
  }
};