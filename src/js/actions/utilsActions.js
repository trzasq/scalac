import axios from 'axios';

export const GET_DATA_FAILED = 'GET_DATA_FAILED';
export const CLEAR_ERRORS = 'CLEAR_ERRORS';
export const SHOW_LOADING = 'SHOW_LOADING';
export const HIDE_LOADING = 'HIDE_LOADING';

export const getDataFailed = (message) => ({ type: GET_DATA_FAILED, message });
export const clearError = () => ({ type: CLEAR_ERRORS })
export const showLoading = () => ({ type: SHOW_LOADING })
export const hideLoading = () => ({ type: HIDE_LOADING })

export function errorHandler() {
  return (dispatch) => {
  dispatch(hideLoading());
    
  dispatch(getDataFailed("Connection Error, Please try again later"));
}}



