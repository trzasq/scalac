import axios from 'axios';
import {uniqBy} from 'lodash';
import {showLoading, hideLoading} from './utilsActions';
import parseLink from 'parse-link-header';
const baseUrl = 'https://api.github.com/';
const token = 'ce3ed89fdfd09bccd5444b40b63f46f16e274318';

export const GET_USERS_DONE = 'GET_USERS_DONE';
export const GET_REPOS_DONE = 'GET_REPOS_DONE';
export const TOGGLE_SORT_DIRECTION = 'TOGGLE_SORT_DIRECTION';
export const SET_USER_DETAILS_ID = 'SET_USER_DETAILS_ID';
export const SET_REPO_DETAILS_ID = 'SET_REPO_DETAILS_ID';
export const SET_FILTER_TYPE = 'SET_FILTER_TYPE';

export const getUsersDone = (users) => ({type: GET_USERS_DONE, users});
export const getReposDone = (repos) => ({type: GET_REPOS_DONE, repos});
export const toggleSortDirection = (direction) => ({type: TOGGLE_SORT_DIRECTION, direction});
export const setUserDetailsId = (id) => ({type: SET_USER_DETAILS_ID, id});
export const setRepoDetailsId = (id) => ({type: SET_REPO_DETAILS_ID, id});
export const setFilterType = (filterName) => ({type: SET_FILTER_TYPE, filterName});

const myPromiseFunction = (repositoryName) => {

  const promise = new Promise((resolve, reject) => {
    return axios.head(`${baseUrl}repos/angular/${repositoryName}/contributors`, {
      params: {
        per_page: 100
      },
      headers: {
        "Authorization": `token ${token}`
      }
    }).then(response => {
      let pagesNum = (response.headers.link !== undefined)
        ? parseLink(response.headers.link).last.page
        : 1;

      let pagesNumArray = Array.from(Array(Number(pagesNum)).keys())

      return axios.all(pagesNumArray.map((index) => {
        let page = index + 1;
        return axios.get(`${baseUrl}repos/angular/${repositoryName}/contributors`, {
          params: {
            per_page: 100,
            page
          },
          headers: {
            "Authorization": `token ${token}`
          }

        })
      })).then(response => {

        let mergeResults = [];
        response.map(resp => {
          if (resp.status === 200 || resp.status === 304) {
            mergeResults = [
              ...mergeResults,
              ...resp.data
            ]
          }

        });

        resolve(mergeResults);
        return mergeResults;

      })
    })
  })
  return promise;
}

export const getAllAngularRepos = () => {
  
  let usersArray = [];
  let reposArray = [];
  let users;
  let contributors;

  return (dispatch) => {
    dispatch(showLoading());
    axios.head(`${baseUrl}orgs/angular/repos`, {
      params: {
        per_page: 100
      },
      headers: {
        "Authorization": `token ${token}`
      }
    }).then(response => {
      if (response.status === 200 || response.status === 304) {

        const pagesNum = response
          .headers
          .link
          .match(/&page=([0-9]+)/)[1];
        const pagesNumArray = Array.from(Array(Number(pagesNum)).keys())

        // axios.all(response.data.map((result) => {
        axios.all(pagesNumArray.map((index) => {
          let page = index + 1;
          return axios.get(`${baseUrl}orgs/angular/repos`, {
            params: {
              per_page: 100,
              page
            },
            headers: {
              "Authorization": `token ${token}`
            }
          })
        })).then(response => {
          let mergeResults = [];
          response.map(resp => {
            if (resp.status === 200 || resp.status === 304) {
              mergeResults = [
                ...mergeResults,
                ...resp.data
              ]
            }

          });
          axios.all(mergeResults.map((result) => {
            return myPromiseFunction(result.name).then(response => {

              let ids = [];
              let usersIds = response.map(user => {
                ids.push(user.id);
                result.usersIds = ids;
              })

              reposArray = [
                ...reposArray, {
                  ...result
                }
              ]
              usersArray = [
                ...usersArray,
                ...response
              ];
              return users
            })
          })).then(res => {
            users = usersArray.map(item => {

              item.angularContributions = 1;
              return item;
            })
            contributors = new Map();
            users.forEach((element) => {
              if (contributors.get(element.id)) 
                contributors.set(element.id, contributors.get(element.id) + element.angularContributions);
              else 
                contributors.set(element.id, element.angularContributions);
              }
            );
            users = _.uniqBy(users, 'id');
            users.map(user => {

              user.angularContributions = contributors.get(user.id);
              return users

            });
            dispatch(getUsersDone(users));
            dispatch(getReposDone(reposArray));
            dispatch(hideLoading());

          }).catch((error) => {
            // dispatch(getDataFailed(error.message));
          });

        })

      }
    });

    //TODO

  }
}