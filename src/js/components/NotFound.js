import React, { Component } from 'react';

import Box from 'grommet/components/Box';


class NotFound extends Component {

  render() {
    return (
      <Box>
        NotFound
            </Box>
    );
  }
}

export default NotFound;