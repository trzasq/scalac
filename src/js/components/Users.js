import React, {Component} from 'react';
import {connect} from 'react-redux';
import {getAllAngularRepos, toggleSortDirection, setUserDetailsId, setFilterType} from '../actions/usersActions';
import {filterSelector, sliceSelector} from '../selectors/sortedList';
import styled from 'styled-components';
import {withRouter} from "react-router-dom";

import Box from 'grommet/components/Box';
import Button from 'grommet/components/Button';
import Header from 'grommet/components/Header';
import Heading from 'grommet/components/Heading';
import List from 'grommet/components/List';
import Layer from 'grommet/components/Layer';
import Sidebar from 'grommet/components/Sidebar';
import Section from 'grommet/components/Section';
import Select from 'grommet/components/Select';
import ListUsersItem from './ListUserItem';
import {Motion, spring} from 'react-motion';
import ComonHeader from './CommonHeader';

import DescendIcon from 'grommet/components/icons/base/Descend';
import AscendIcon from 'grommet/components/icons/base/Ascend';

const StyledIconDesc = styled(DescendIcon)`
stroke: ${props => props.sort === 'desc'
  ? 'palevioletred'
  : '#333'};
`;

const StyledIconAsc = styled(AscendIcon)`
stroke: ${props => props.sort === 'asc'
  ? 'palevioletred'
  : '#333'};
`;

class Users extends Component {

  constructor() {
    super()

    this.state = {
      filterLayer: false,
      limit: 100,
      loadMore: true
    }

    this._filterUsers = this
      ._filterUsers
      .bind(this);
    this._onChangeDirection = this
      ._onChangeDirection
      .bind(this);
    this._onShowUserDetails = this
      ._onShowUserDetails
      .bind(this);
    this._onChangefilterType = this
      ._onChangefilterType
      .bind(this);
    this._onMoreData = this
      ._onMoreData
      .bind(this);

  }

  componentDidMount() {

    const {users, getAllAngularRepos} = this.props;

    if (!users.length) {
      getAllAngularRepos();

    }

  }

  _onChangeDirection(direction) {
    const {toggleSortDirection} = this.props;
    this.setState((state)=>({ limit: 100 }), ()=>{
      toggleSortDirection(direction);
    });
    

  }

  _onChangefilterType(event) {
    const {setFilterType} = this.props;
    const {value} = event.value;
    this.setState((state)=>({ limit: 100 }), ()=>{
      setFilterType(value);
    });
    

    
  }

  _filterUsers() {
    this.setState({
      filterLayer: !this.state.filterLayer
    })

  }

  _onShowUserDetails(event) {
    const {userId} = event.currentTarget.dataset;
    const {setUserDetailsId} = this.props;
    setUserDetailsId(userId);

    this
      .props
      .history
      .push(`/user/${userId}`);

  }

  _onMoreData(event) {
    const { length } =  this.props.users;
    
    this.setState((state)=>({ limit: this.state.limit + 50 }), ()=>{
      let loadMore = (this.state.limit >= length) ? false : true; 
    });
  }
  render() {
    const {users, repos, sort, toggleSortDirection, selectedUser, limit} = this.props;

    let limitedUsers = users.slice(0,this.state.limit);

    let filter = (this.state.filterLayer)
      ? <Layer flush={true} align='right' closer={true} onClose={this._filterUsers}>
          <div>
            <Header
              size='large'
              justify='between'
              align='center'
              pad={{
              horizontal: 'medium',
              vertical: 'medium'
            }}>
              <Heading tag='h2' margin='none'>Filter</Heading>

            </Header>

            <Section
              pad={{
              horizontal: 'large',
              vertical: 'small'
            }}>
              <Heading tag='h3'>Sort</Heading>
              <Box direction='row' justify='start' align='center' responsive={false}>
                <Select
                  inline={true}
                  onChange={this._onChangefilterType}
                  options={[
                  {
                    label: "Angular Contributions",
                    value: 'angularContributions'
                  }, {
                    label: "Contributions",
                    value: 'contributions'
                  }
                ]}
                  value={sort.type}/>
                <Box direction='row' flex={false} responsive={false} align='center'>

                  <Button
                    onClick={this
                    ._onChangeDirection
                    .bind(this, 'desc')}
                    icon={< StyledIconDesc sort = {
                    sort.direction
                  } />}/>
                  <Button
                    icon={< StyledIconAsc sort = {
                    sort.direction
                  } />}
                    onClick={this
                    ._onChangeDirection
                    .bind(this, 'asc')}/>
                </Box>
              </Box>
            </Section>
          </div>
          <Sidebar></Sidebar>

        </Layer>
      : null

    return (
      <Motion
        defaultStyle={{
        opacity: 0.1
      }}
        style={{
        opacity: spring(1)
      }}>
        {(style) => (
          <Box style={{
            opacity: style.opacity
          }} flex={true}>
            <ComonHeader onFilterIcon={this._filterUsers} title="Users"/> {(limitedUsers.length > 0) && <List onMore={this._onMoreData}>{limitedUsers.map((user) => {
                return <ListUsersItem
                  contributions={user.contributions}
                  dataUserId={user.id}
                  onClick={this._onShowUserDetails}
                  name={user.login}
                  imageUrl={user.avatar_url}
                  key={user.id}
                  angularContributions={user.angularContributions}/>
              })}
            </List>}

            {filter}

          </Box>
        )}

      </Motion>
    );
  }
}

const mapStateToProps = (state) => ({ limit: state.usersReducer.listLimit, selectedUser: state.usersReducer.selectedUser, sort: state.usersReducer.sort, users: filterSelector(state), repos: state.usersReducer.repos})
const mapDispatchToProps = (dispatch) => ({
  getAllAngularRepos: () => dispatch(getAllAngularRepos()),
  toggleSortDirection: (direction) => dispatch(toggleSortDirection(direction)),
  setUserDetailsId: (id) => dispatch(setUserDetailsId(id)),
  setFilterType: (type) => dispatch(setFilterType(type))
})

export default Users = withRouter(connect(mapStateToProps, mapDispatchToProps)(Users));
