import React, {Component} from 'react';

import Box from 'grommet/components/Box';
import Animate from 'grommet/components/Animate';
import Heading from 'grommet/components/Heading';
import Paragraph from 'grommet/components/Paragraph';
import Anchor from 'grommet/components/Anchor';
import Button from 'grommet/components/Button';
import Section from 'grommet/components/Section';
import styled, {keyframes} from 'styled-components';



class About extends Component {

  render() {
    return (
      <Section
        full={true}
        id="about"
        flex={true}
        colorIndex="light-2"
        justify="between"
        align="center"
        separator="top">

        <Heading tag='h2' strong={true}>Release Notes</Heading>

        <Animate
          visible={true}
          keep={true}
          enter={{
          animation: 'fade',
          duration: 1000,
          delay: 100
        }}>

          <Box pad='large' align='center' justify='center'>
           
          </Box>
          <Paragraph>
            This app is build on top of grommet cli for rapid prototyping puposes. 
            Type <b>npm run dev</b> to start devserver 
            <b> npm run dist</b> to go production
          </Paragraph>
          <Paragraph>
          I leave access_token in because it is also in history commit, but please do not hack me or generate your own token ;)
          The fetching data takes aprox 20s. I store the data in redux to avoid many requests. There should be done also comparing time request to force 304
          </Paragraph>
          <Paragraph>
          I tried to make it fully functionality like in specification. I belive it only lacks sorting on follower and gists. I didn't do this because it is going to exhaust x-rate-limit of Github limits;
          but selector is ready for this and needs only to put an object key in select input
          </Paragraph>
          <Heading  tag='h4' strong={true}>//TODO </Heading>
          
          <Paragraph>
          
          </Paragraph>
          <Paragraph>
          Due to limited time it lacks of test. Also probably using graphQL would be better and less heavy. But i dont know github api very much. So decided go simpler way
          </Paragraph>
          

        </Animate>
      </Section>
    );
  }
}

export default About;