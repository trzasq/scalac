import React, { Component } from 'react';

import Box from 'grommet/components/Box';
import Button from 'grommet/components/Button';
import Header from 'grommet/components/Header';
import Title from 'grommet/components/Title';
import Menu from 'grommet/components/Menu';
import Search from 'grommet/components/Search';
import FilterIcon from 'grommet/components/icons/base/Filter';
import ContactInfoIcon from 'grommet/components/icons/base/ContactInfo';

import propTypes from 'prop-types';


class CommonHeader extends Component {

  render() {
    const { title, onFilterIcon, search, titleIcon } = this.props
    return (
      <Header
        fill={true}
        pad='small'
        separator='bottom'
        fixed={true}

        size='medium'>
        <Title>
          {titleIcon}
          {title}
        </Title>
        <Box flex={true}
          justify='end'
          direction='row'
          responsive={false}>
          {search && <Search inline={true}
            fill={true}
            size='medium'
            placeHolder='Search'
            dropAlign={{ "right": "right" }} />}
          <Box>
            {onFilterIcon && <Button icon={<FilterIcon />}
              onClick={onFilterIcon}
              plain={true} /> }
            
          </Box>

        </Box>
      </Header>

    );
  }
}

CommonHeader.propTypes = {
  title: propTypes.string.isRequired,
 onFilterIcon: propTypes.func,
 search: propTypes.bool
}

export default CommonHeader;
