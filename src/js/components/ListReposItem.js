
import React from 'react';
import ListItem from 'grommet/components/ListItem';
import Animate from 'grommet/components/Animate';
import Box from 'grommet/components/Box';
import Image from 'grommet/components/Image';
import UserIcon from 'grommet/components/icons/base/User';
import propTypes from 'prop-types';


const ListRepoItem= (props) => {
  
  
  return (
   
    <ListItem justify="between" onClick={props.onClick}
      data-repo-id={props.dataRepoId}
      pad={{horizontal: 'medium', vertical: 'small', between: 'medium'}}
      
      colorIndex={props.colorIndex}>
      <Box pad={{between: 'small'}} direction="row" align="center"
        responsive={false} className="flex">
        <Image size="thumb" mask={true}
        src={props.imageUrl} />
        <span>{props.name}</span>
      </Box>
      <span className="secondary">
       {props.angularContributions}
      </span>
    </ListItem>
  );
};

ListRepoItem.propTypes = {

  onClick: propTypes.func,
  imageUrl: propTypes.string,
  dataRepoId: propTypes.number.isRequired
};

export default ListRepoItem;