
import React from 'react';
import ListItem from 'grommet/components/ListItem';
import Animate from 'grommet/components/Animate';
import Box from 'grommet/components/Box';
import Image from 'grommet/components/Image';
import UserIcon from 'grommet/components/icons/base/User';
import propTypes from 'prop-types';
import Value from 'grommet/components/Value';
import GlobeIcon from 'grommet/components/icons/base/Globe';
import PlatformGoogleIcon from 'grommet/components/icons/base/PlatformGoogle';

const ListUserItem = (props) => {
  
  
  return (
   
    <ListItem justify="between" onClick={props.onClick}
      data-user-id={props.dataUserId}
      pad={{horizontal: 'medium', vertical: 'small', between: 'medium'}}
      
      colorIndex={props.colorIndex}>
      <Box>
      <Box pad={{between: 'small'}} direction="row" align="center"
        responsive={false} className="flex">
        <Image size="thumb" mask={true}
        src={props.imageUrl} />
        <span>{props.name}</span>
      </Box>
      </Box>
      <Box direction='row' justify="between" align='center'>
        <Box pad={{horizontal:'small'}}>
        <Value pad='small' direction='row' label='Projects contibutions:' align='center' size='small' value={props.contributions}
  icon={<GlobeIcon />} />
  </Box>
  <Box pad={{horizontal:'small'}}>
  <Value direction='row' label='Anuglar contibutions:' align='center' size='small' value={props.angularContributions}
  icon={<PlatformGoogleIcon />} />
       
       
      </Box>
      </Box>
    </ListItem>
  );
};

ListUserItem.propTypes = {

  onClick: propTypes.func,
  imageUrl: propTypes.string,
  dataUserId: propTypes.number.isRequired
};

export default ListUserItem;