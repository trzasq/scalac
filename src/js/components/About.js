import React, {Component} from 'react';

import Box from 'grommet/components/Box';
import Animate from 'grommet/components/Animate';
import Heading from 'grommet/components/Heading';
import Paragraph from 'grommet/components/Paragraph';
import Anchor from 'grommet/components/Anchor';
import Button from 'grommet/components/Button';
import Section from 'grommet/components/Section';
import styled, {keyframes} from 'styled-components';

const scaleUpFadeOut = keyframes `
 0% {
    
    transform: scale(1);
    opacity: 1;
  }

  15% {
    opacity: 1;
  }

  100% {
    transform: scale(1.5);
    opacity: 0;
  }`;

const VeryStyledButton = styled(Button)`
box-shadow:0px 0px 0px 2px #865CD6;
  position: absolute;
  opacity: 0;
  top:0;
  bottom: 0;
  left: 0;
  right: 0;
    box-sizing: border-box;
    transform-origin: center;
    border: 1px solid;
	border-color: #57C7D4;
 animation: ${scaleUpFadeOut} 1.2s 0.3s infinite
`;

const StyledButton = styled(Button)`
border: none;
opacity: 1;
`

class About extends Component {

  render() {
    return (
      <Section
        full={true}
        id="about"
        flex={true}
        colorIndex="light-2"
        justify="center"
        align="center"
        separator="top">
        <Heading tag='h2' strong={true}>Getting started</Heading>
        <Animate
          visible={true}
          keep={true}
          enter={{
          animation: 'fade',
          duration: 1000,
          delay: 100
        }}>
          <Box pad='large' align='center' justify='center'>
            <Box style={{
              'position': 'relative'
            }}>
              <StyledButton label='Start'/>
              <VeryStyledButton path='/users'/>
            </Box>
          </Box>
          <Box align='center' pad='large'>
            <Paragraph align='center'>
              This app provides all the guidance to seek and manage users of specific
              repository This example is based on all angular repositories.
            </Paragraph>
          </Box>
        </Animate>
      </Section>
    );
  }
}

export default About;