import React, { Component } from 'react';
import { connect } from 'react-redux';
import { filterSelector } from '../selectors/sortedList';
import { getAllAngularRepos, toggleSortDirection, setRepoDetailsId } from '../actions/usersActions';
import styled from 'styled-components';
import { withRouter } from "react-router-dom";
import { specificRepoSelector, usersBelongsToSpecificRepo } from '../selectors/userRepos';

import Box from 'grommet/components/Box';
import Button from 'grommet/components/Button';
import Header from 'grommet/components/Header';
import Heading from 'grommet/components/Heading';
import List from 'grommet/components/List';
import LinkNextIcon from 'grommet/components/icons/base/LinkNext';
import Section from 'grommet/components/Section';
import Title from 'grommet/components/Title';
import ListUsersItem from './ListUserItem';
import { Motion, spring } from 'react-motion';
import ComonHeader from './CommonHeader';



class RepositoryDetails extends Component {

  constructor(props) {
    super(props)

    this.state = {
      limit: 100,
      loadMore: true
    }
    this.repoId = this.props.match.params.id;

    this._onShowUserDetails = this.
      _onShowUserDetails.bind(this);
    this._onMoreData = this
      ._onMoreData
      .bind(this);

  }




  _onShowUserDetails(event) {
    const { userId } = event.currentTarget.dataset;
    this.props.history.push(`/user/${userId}`);

  }
  _onMoreData(event) {
    const { length } = this.props.usersList;

    this.setState((state) => ({ limit: this.state.limit + 50 }), () => {
      let loadMore = (this.state.limit >= length === true) ? false : true;
      this.setState({loadMore})
    });
  }
  render() {
    const { users, repos, repository, selected, usersList } = this.props;

    let limitedUsers = usersList.slice(0, this.state.limit);


    return (
      <Motion defaultStyle={{ opacity: 0.1 }}
        style={{ opacity: spring(1) }} >
        {(style) => (
          <Box style={{ opacity: style.opacity }} flex={true}>
            <Header
              fill={true}
              pad='small'
              separator='bottom'
              fixed={true}

              size='medium'>
              <Title>
                {repository.name}
                <LinkNextIcon />
                Users

              </Title>
            </Header>
            {(limitedUsers.length > 0) && <List onMore={this.state.loadMore ? this._onMoreData: null}>{limitedUsers.map((user) => {
              return <ListUsersItem contributions={user.contributions} dataUserId={user.id} onClick={this._onShowUserDetails} name={user.login} imageUrl={user.avatar_url} key={user.id} angularContributions={user.angularContributions} />
            })} </List>}



          </Box>
        )}

      </Motion>
    );
  }
}

const mapStateToProps = (state) =>
  ({

    repository: specificRepoSelector(state),
    users: filterSelector(state),
    selected: state.usersReducer.selectedRepo,
    repos: state.usersReducer.repos,
    usersList: usersBelongsToSpecificRepo(state)
  })
const mapDispatchToProps = (dispatch) =>
  ({
    setRepoDetailsId: (id) => { dispatch(setRepoDetailsId(id)) }
  })



export default RepositoryDetails = withRouter(connect(mapStateToProps, mapDispatchToProps)(RepositoryDetails));
