import React, {Component} from 'react';
import {connect} from 'react-redux';
import Users from './Users';
import Section from 'grommet/components/Section';
import Heading from 'grommet/components/Heading';
import Animate from 'grommet/components/Animate';
import Box from 'grommet/components/Box';
import styled, {keyframes} from 'styled-components';

const flashingDots = keyframes `
50% { color: transparent }}`;

const Dot = styled.span`

&:nth-child(3) {
  animation: 1s ${flashingDots}  .5s infinite;
}
&:nth-of-type(2)  {
  animation: 1s ${flashingDots}  .25s infinite;
}
&:nth-of-type(1) {
  animation: ${flashingDots} 1s  infinite;
}
`;






export default function () {

    class Preloader extends Component {
        render() {

            const {isLoading} = this.props;
            console.log(isLoading)
            
            let preloader = (
                <Section
                    full={true}
                    colorIndex="light-2"
                    pad="none"
                    justify="center"
                    align="center">

                    <Heading tag="h1">Loading <Dot>.</Dot><Dot>.</Dot><Dot>.</Dot>
                    </Heading>

                </Section>
            );

            let main = (
                <Users/>
            )

            return (isLoading)
                ? preloader
                : main;

        };
    }

    function mapStateToProps(state) {
        return {isLoading: state.utilsReducer.isLoading};
    }

    return connect(mapStateToProps)(Preloader);
}