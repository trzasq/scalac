import React, {Component} from 'react';
import {connect} from 'react-redux';
import {getAllAngularRepos, setUserDetailsId, setRepoDetailsId} from '../actions/usersActions';
import styled from 'styled-components';
import {userSelector} from '../selectors/chosenUser';
import {userReposSelector} from '../selectors/userRepos';
import {withRouter} from "react-router-dom";

import Anchor from 'grommet/components/Anchor';
import Box from 'grommet/components/Box';
import Button from 'grommet/components/Button';
import Card from 'grommet/components/Card';
import Header from 'grommet/components/Header';
import Heading from 'grommet/components/Heading';
import Image from 'grommet/components/Image';
import List from 'grommet/components/List';
import Layer from 'grommet/components/Layer';
import Paragraph from 'grommet/components/Paragraph';
import Sidebar from 'grommet/components/Sidebar';
import Section from 'grommet/components/Section';
import Select from 'grommet/components/Select';
import Split from 'grommet/components/Split';
import ListReposItem from './ListReposItem';
import {Motion, spring} from 'react-motion';
import ComonHeader from './CommonHeader';

import DescendIcon from 'grommet/components/icons/base/Descend';
import AscendIcon from 'grommet/components/icons/base/Ascend';
import ContactInfoIcon from 'grommet/components/icons/base/ContactInfo';

class UserDetails extends Component {

  constructor(props) {
    super(props);

    this.userId = this.props.match.params.id;
    this._onShowContributors = this
      ._onShowContributors
      .bind(this)

    this.state = {
      filterLayer: false
    }

  }

  componentWillMount() {
    const {setUserDetailsId} = this.props;

    setUserDetailsId(this.userId);
  }

  _onShowContributors(event) {
    const {setRepoDetailsId} = this.props;
    const {repoId} = event.currentTarget.dataset;
    setRepoDetailsId(repoId);
    this
      .props
      .history
      .push(`/repo/${repoId}`);

  }

  render() {
    const {repos, user, selectedUser, users, userRepositories} = this.props;

    console.log(selectedUser)

    return (
      <Motion
        defaultStyle={{
        opacity: 0.1
      }}
        style={{
        opacity: spring(1)
      }}>
        {(style) => (
          <Box style={{
            opacity: style.opacity
          }} flex={true}>
            <Split priority='both' separator={true} flex='right'>
              <Box><ComonHeader
                titleIcon={< ContactInfoIcon size = 'medium' />}
                title="User Details"/>
                <Card
                  thumbnail={user.avatar_url}
                  label={user.type}
                  heading={user.login}
                  description='Sample description providing more details.'
                  link={< Anchor target = '_blank' href = {
                  user.html_url
                }
                label = 'See On Github' />}/>

              </Box>

              <Box><ComonHeader title="Repositories"/> {(userRepositories.length > 0) && <List>{userRepositories.map((repo) => {
                    return <ListReposItem
                      imageUrl='/img/angular.png'
                      dataRepoId={repo.id}
                      name={repo.name}
                      onClick={this._onShowContributors}
                      key={userRepositories.id}/>
                  })}
                </List>}
              </Box>

            </Split>

          </Box>
        )}

      </Motion>
    );
  }
}

const mapStateToProps = (state) => {
  console.log(state.usersReducer.selectedUser)
  return {selectedUser: state.usersReducer.selectedUser, repos: state.usersReducer.repos, user: userSelector(state), userRepositories: userReposSelector(state)}
}
const mapDispatchToProps = (dispatch) => ({
  getAllAngularRepos: () => dispatch(getAllAngularRepos()),
  setUserDetailsId: (id) => dispatch(setUserDetailsId(id)),
  setRepoDetailsId: (id) => dispatch(setRepoDetailsId(id)),
})

export default UserDetails = withRouter(connect(mapStateToProps, mapDispatchToProps)(UserDetails));
