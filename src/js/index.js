import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import store from './store';

import '../scss/index.scss';

import App from './App';

const element = document.getElementById('content');
ReactDOM.render(<Provider store={store}><App /></Provider>, element);

document.body.classList.remove('loading');
